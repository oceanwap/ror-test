class ArticlesController < ApplicationController

	def show
		article = Article.find_by(id: params[:id])
		render json: article, :include => :owner
	end
end
