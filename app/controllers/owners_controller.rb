class OwnersController < ApplicationController
	def show
		owner = Owner.find_by(name: params[:owner_name])
		render json: owner
	end

	def articles
		@owner = Owner.find_by(name: params[:owner_name])
		render json: @owner, :include => :article
	end

	def index
		owners = Owner.all
		render json: { :owners => owners }
	end
end
