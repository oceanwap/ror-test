class CreateOwners < ActiveRecord::Migration
  def up
    create_table :owners do |t|
      t.string :name
      t.timestamps null: false
    end

    add_index :owners, :name, :unique => true

    create_table :articles do |t|
      t.string :name
      t.string :price
      t.string :description
      t.integer :owner_id
      t.timestamps null: false
    end

    add_foreign_key :articles, :owners
  end

  def down
  	drop_table :articles
  	drop_table :owners
  end
end
